package httpHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.RequestLine;
import org.apache.http.util.EntityUtils;
import org.apache.lucene.document.Document;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Indexer.Keywords;

public class ElasticSearchRequest {
	
	String urlElasticSearch = "http://inception.is.inf.uni-due.de:20001/_search";
	
	static Keywords keywords;
	
	public ElasticSearchRequest()
	{
		keywords = new Keywords();
		
		searchRequest("staff", 2);
	}
	
	public JSONObject sendRequest(String pApi, String pQuery)
	{
		RestClient restClient = RestClient.builder(new HttpHost("inception.is.inf.uni-due.de", 20001, "http")).build();
		JSONObject resObject = null;
		
		try {
			Request request = new Request("POST", pApi);
			request.setJsonEntity(pQuery);
			//System.out.println(pQuery);
			Response response = restClient.performRequest(request);
			RequestLine requestLine = response.getRequestLine(); 
			String responseBody = EntityUtils.toString(response.getEntity());
			
			JSONParser parser = new JSONParser();
			resObject = (JSONObject) parser.parse(responseBody);
			
			restClient.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	    return resObject;
	}
	
	
	public String searchRequest(String pQuery, int pTopDocs)
	{
		String topic = keywords.getRelatedTopic(pQuery);
		List <String> aspectsKeywords = keywords.getRelatedKeywords(topic);
		List<Document> documents = new ArrayList<Document>();
		
		if(aspectsKeywords == null)
		{
			aspectsKeywords = new ArrayList<String>();
			aspectsKeywords.add(pQuery);
		}
		
		// Get Positive
		String termsPositive = "";
		String termsNegative = "";
		
		for(int i=0;i<aspectsKeywords.size();i++)
		{
			String keyword = aspectsKeywords.get(i);
			boolean isLast = i == (aspectsKeywords.size()-1);
			termsPositive = termsPositive + "{\"term\" : { \"rev_plus\" : \""+keyword+"\" }}" + (isLast?"":",") +"\n";
			termsNegative = termsNegative + "{\"term\" : { \"rev_minus\" : \""+keyword+"\" }}" + (isLast?"":",") +"\n";
		}
		
		//String hotelID = "1";
		/*
		String getPositiveReviewsQuery = "{" + 
				                          "  \"query\": {" + 
				                          "    \"bool\": {" + 
				                          "      \"should\": [" + termsPositive +
				                          "      ],\"minimum_should_match\": 1,\r\n" + 
				                          "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }] " +
				                          " }"+
				                          "  },\r\n" + 
				                          "  \"_source\": [\"h_id\"],\r\n" + 
				                          "  \"from\" : 0, \"size\" : 10000\r\n" +
				                          "}";*/
		/*
		String getPositiveReviewsCount = "{" + 
                "  \"query\": {" + 
                "    \"bool\": {" + 
                "      \"should\": [" + termsPositive +
                "      ],\"minimum_should_match\": 1,\r\n" + 
                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" }}, {\"term\":{\"h_id\":\""+hotelID+"\"}}] " +
                " }"+
                "  }" + 
                "}";
		*//*
		String termAggregationCount = "{\r\n" + 
				"  \"size\": 0,\r\n" + 
				"  \"query\": {" + 
                "    \"bool\": {" + 
                "      \"should\": [" + termsPositive +
                "      ],\"minimum_should_match\": 1,\r\n" + 
                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }, {\"term\":{\"h_id\":\""+hotelID+"\"}} ] " +
                " }"+
                "}" + *//*","+
                "\"aggs\": {\r\n" + 
                "    \"uniqueID\": {\r\n" + 
                "      \"terms\": { \"field\": \"r_id\" }\r\n" + 
                "    }\r\n" + 
                "  }" +*/
               /* "}";*/
		
		/*
		String getPositiveReviewsQuery = "{" + 
                "  \"query\": {" + 
                "    \"bool\": {" + 
                "    \"must\": [ "+
                "\"bool\": {\r\n" + 
                "          \"should\": [" + termsPositive +
                "         ], \"minimum_number_should_match\": 1 }," +
                "{ \"term\" : { \"_index\" : \"bcom_dataset_review\" } } " +
                "] }"+
                
                "  },\r\n" + 
                "  \"_source\": [\"h_id\"],\r\n" + 
                "  \"from\" : 0, \"size\" : 10000\r\n" +
                "}";
		*/
		
		int maxHotels = 3;
		for(int i=0;i<maxHotels;i++)
		{
			String hotelID = ""+(i+1);
			String termAggregationCountP = "{" +
					"  \"size\": 0," +
					"  \"query\": {" +
	                "    \"bool\": {" +
	                "      \"should\": [" + termsPositive +
	                "      ],\"minimum_should_match\": 1," +
	                "      \"must_not\": [" + termsNegative + "],"+
	                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }, {\"term\":{\"h_id\":\""+hotelID+"\"}} ] " +
	                " }"+
	                "}" + /*","+
	                "\"aggs\": {" +
	                "    \"uniqueID\": {" +
	                "      \"terms\": { \"field\": \"r_id\" }" +
	                "    }" + 
	                "  }" +*/
	                "}";
			
			String termAggregationCountN = "{" +
					"  \"size\": 0," +
					"  \"query\": {" +
	                "    \"bool\": {" +
	                "      \"should\": [" + termsNegative +
	                "      ],\"minimum_should_match\": 1," +
	                "      \"must_not\": [" + termsPositive + "],"+
	                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }, {\"term\":{\"h_id\":\""+hotelID+"\"}} ] " +
	                " }"+
	                "}" + /*","+
	                "\"aggs\": {" + 
	                "    \"uniqueID\": {" + 
	                "      \"terms\": { \"field\": \"r_id\" }" + 
	                "    }" + 
	                "  }" +*/
	                "}";
			
			String termUnknown = "{" +
					"  \"size\": 0," +
					"  \"query\": {" +
	                "    \"bool\": {" +
	                "      \"must_not\": [" + termsPositive + ","+termsNegative +
	                "      ]," +
	                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }, {\"term\":{\"h_id\":\""+hotelID+"\"}} ] " +
	                " }"+
	                "}" + /*","+
	                "\"aggs\": {" + 
	                "    \"uniqueID\": {" + 
	                "      \"terms\": { \"field\": \"r_id\" }" + 
	                "    }" + 
	                "  }" +*/
	                "}";
			
			String allReviews = "{" +
					"\"size\":\"0\"," +
					"\"query\": {" +
					"\"bool\": {" +
					"\"must\": [" +
					"{" +
					"\"match\": {" +
					"\"h_id\": \""+hotelID+"\"" +
					"}" +
					"}," +
					"{" +
					"\"match\": {" +
					"\"_index\": \"bcom_dataset_review\"" +
					"}}]}}}";
			
			String scoresQueryP = "{" +
					"\"size\":\"10000\"," +
					"  \"query\": {" +
	                "    \"bool\": {" +
	                "      \"should\": [" + termsPositive +
	                "      ],\"minimum_should_match\": 1," +
	                "      \"must_not\": [" + termsNegative + "],"+
	                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }, {\"term\":{\"h_id\":\""+hotelID+"\"}} ] " +
	                " }"+
	                "}" + ","+
	                "\"_source\": [\"score\"]" +
	                "}";
			
			String scoresQueryN = "{" +
					"\"size\":\"10000\"," +
					"  \"query\": {" +
	                "    \"bool\": {" +
	                "      \"should\": [" + termsNegative +
	                "      ],\"minimum_should_match\": 1," +
	                "      \"must_not\": [" + termsPositive + "],"+
	                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }, {\"term\":{\"h_id\":\""+hotelID+"\"}} ] " +
	                " }"+
	                "}" + ","+
	                "\"_source\": [\"score\"]" +
	                "}";
			
			
			String scoresQueryU = "{" +
					"  \"size\": 10000," +
					"  \"query\": {" +
	                "    \"bool\": {" +
	                "      \"must_not\": [" + termsPositive + ","+termsNegative +
	                "      ]," +
	                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }, {\"term\":{\"h_id\":\""+hotelID+"\"}} ] " +
	                " }"+
	                "}," +
	                "\"_source\": [\"score\"]" +
	                "}";
			
			String scoresQueryI = "{" +
					"\"size\":\"10000\"," +
					"  \"query\": {" +
	                "    \"bool\": {" +
	                "      \"should\": [" + termsPositive + "," + termsNegative +
	                "      ],\"minimum_should_match\": 1," +
	                "    \"must\": [ { \"term\" : { \"_index\" : \"bcom_dataset_review\" } }, {\"term\":{\"h_id\":\""+hotelID+"\"}} ] " +
	                " }"+
	                "}" + ","+
	                "\"_source\": [\"score\"]" +
	                "}";
			/*
			JSONObject resP = sendRequest("_search", termAggregationCountP);
			JSONObject resN = sendRequest("_search", termAggregationCountN);
			JSONObject resU = sendRequest("_search", termUnknown);
			JSONObject resA = sendRequest("_search", allReviews);
			*/
			//JSONObject resScoreQueryP = sendRequest("_search", scoresQueryP);
			//JSONObject resScoreQueryN = sendRequest("_search", scoresQueryN);
			
			if(true) // for testing
			{
				JSONObject resScoreQueryU = sendRequest("_search?filter_path=hits.total,hits.hits._source", scoresQueryU);
				JSONObject resScoreQueryP = sendRequest("_search?filter_path=hits.total,hits.hits._source", scoresQueryP);
				JSONObject resScoreQueryN = sendRequest("_search?filter_path=hits.total,hits.hits._source", scoresQueryN);
				JSONObject resScoreQueryI = sendRequest("_search?filter_path=hits.total,hits.hits._source", scoresQueryI);
				
				// Calculate the average scores for each set of Positive, N, U and I reviews
				double scoreAvgU = calculateAvgScore(resScoreQueryU);
				double scoreAvgP = calculateAvgScore(resScoreQueryP);
				double scoreAvgN = calculateAvgScore(resScoreQueryN);
				double scoreAvgI = calculateAvgScore(resScoreQueryI);
				double scoreAvg = (scoreAvgU + scoreAvgP + scoreAvgN + scoreAvgI)/4;
				
				/*
				System.out.println("This test is only done for the first hotel.");
				System.out.println("We retrieve all reviews that matched the undefined query, get their scores from es and calculate scoreU.");
				System.out.println("calculateScoreU returns " + scoreU);
				System.out.println("Custom score calculation for the first "+ maxHotels +" hotels :");*/
				//System.out.println("Hotel " + i + " : scoreAvgU: "+ scoreAvgU + ", scoreAvgP: " + scoreAvgP +
				//		           ", scoreAvgN: " + scoreAvgN + ", scoreAvgI: " + scoreAvgI);
				
				// Get total hits
				int totalU = Integer.valueOf(((JSONObject)resScoreQueryU.get("hits")).get("total").toString());
				int totalP = Integer.valueOf(((JSONObject)resScoreQueryP.get("hits")).get("total").toString());
				int totalN = Integer.valueOf(((JSONObject)resScoreQueryN.get("hits")).get("total").toString());
				int totalI = Integer.valueOf(((JSONObject)resScoreQueryI.get("hits")).get("total").toString());
				double total = (double)(totalU + totalP + totalN + totalI);
				
				double boost = 2f;
				
				// Now we boost P, N, U and I reviews, based on their score
				// Todo: a positive score that is BELOW average, should be boosted LESS, not also more. We can ignore this for now
				double probBoostU = totalU * (1+Math.abs(scoreAvgU-scoreAvg)*boost);
				double probBoostP = totalP * (1+Math.abs(scoreAvgP-scoreAvg)*boost);
				double probBoostN = totalN * (1+Math.abs(scoreAvgN-scoreAvg)*boost);
				double probBoostI = totalI * (1+Math.abs(scoreAvgI-scoreAvg)*boost);
				double probBoostTotal = probBoostU + probBoostP + probBoostN + probBoostI;
				probBoostU = probBoostU / probBoostTotal;
				probBoostP = probBoostP / probBoostTotal;
				probBoostN = probBoostN / probBoostTotal;
				probBoostI = probBoostI / probBoostTotal;
				
				double probU = (double)totalU/total;
				double probP = (double)totalP/total;
				double probN = (double)totalN/total;
				double probI = (double)totalI/total;
				
				//System.out.println("Hotel " + i + " : totalU: "+ totalU + ", totalP: " + totalP + ", totalN: " + totalN + ", totalI: " + totalI);
				
				// Now calculate scores
				double scoreU = scoreAvgU;
				double scoreP = 0;
				double scoreN = 0;
				double scoreI = 0;
				
				
				double finalScore =  6.42 + 2.82 * probP - 8.02 * probN + 2.02 * probU  + 1.79 * total;
				double finalScoreBoost =  6.42 + 2.82 * probBoostP - 8.02 * probBoostN + 2.02 * probBoostU  + 1.79 * total; 
				
				System.out.println("Hotel " + i + " : scoreAvgU: "+ scoreAvgU + ", scoreAvgP: " + scoreAvgP +
						    		               ", scoreAvgN: " + scoreAvgN + ", scoreAvgI: " + scoreAvgI + ", average score : " + scoreAvg);
				System.out.println("Hotel " + i + " : probU: "+ probU + ", probP: " + probP +", probN: " + probN + ", probI: " + probI);
				System.out.println("Hotel " + i + " : probBoostU: "+ probBoostU + ", probBoostP: " + probBoostP +
						                              ", probBoostN: " + probBoostN + ", probBoostI: " + probBoostI);
				System.out.println("final score for hotel "+ i + " : " + finalScore + " finalScoreBoosted : " + finalScoreBoost);
				/*
				System.out.println("Hotel " + i + " : scoreU: "+ scoreU + ", scoreP: " + scoreP +
				           ", scoreN: " + scoreN + ", scoreI: " + scoreI);*/
			}
			
			/*
			int totalHitsP = getTotalHits(resP);
			int totalHitsN = getTotalHits(resN);
			int totalHitsU = getTotalHits(resU);
			int totalHitsA = getTotalHits(resA);
			int totalHitsI = totalHitsA - totalHitsP- totalHitsN- totalHitsU;
			
			double probP = (double)totalHitsP/(double)totalHitsA;
			double probN = (double)totalHitsN/(double)totalHitsA;
			double probU =(double)totalHitsU/(double)totalHitsA;
			double probI = 1-(double)(totalHitsP + totalHitsN + totalHitsU)/totalHitsA;
			
			System.out.println("Hotel " + hotelID + "-> P : " + probP + " ("+totalHitsP+")" +
					           ", N : " + probN + " (" + totalHitsN + ")" +
					           ", U : " + probU + " (" + totalHitsU + ")" +
					           ", I : " + probI + " (" + totalHitsI + ")" +
					           ", Total : " + totalHitsA);*/
		}
		
		// No documents found
		if(documents.size() == 0)
		{
			//System.out.println("Query failed! No results");
			System.out.println("For now: we don't give back any search results yet.");
			return "[]";
		}
		
		JSONArray responseArray = new JSONArray();
		
		for(Document d : documents)
		{
			//responseArray.add(documentToJson(d, pQuery));
		}
		
		System.out.println("Query result: "+ responseArray.toString());
		return responseArray.toString();
	}
	
	private double calculateAvgScore(JSONObject pObj)
	{
		JSONObject hits = (JSONObject) pObj.get("hits");
		JSONArray hitsArray = (JSONArray) hits.get("hits");
		
		double tempScore = 0f;
		int count = 0;
		
		for(Object entry : hitsArray)
		{
			if(entry instanceof JSONObject);
			else continue;
			
			JSONObject entryJsonObj = (JSONObject)entry;
			JSONObject source = (JSONObject) entryJsonObj.get("_source");
			double score = (double) source.get("score");
			
			// clamp score
			if(score > 10f) score = 10f;
			if(score < 0) score = 0.0f;
			
			// We calculate the score. We multiply it on top of all previous scores
			tempScore += score;
			count++;
		}
		
		return (tempScore)/(double)count;
	}
	
	private int getTotalHits(JSONObject pObj)
	{
		JSONObject hits = (JSONObject) pObj.get("hits");
		String hitsTotal = (String) hits.get("total").toString();
		
		return Integer.valueOf(hitsTotal);
	}
	
}
