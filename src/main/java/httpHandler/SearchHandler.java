package httpHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import Indexer.IndexerLucene;
import Indexer.IndexerMain;
import Indexer.SentimentIndexer;
import reviewServer.ReviewServer;

public class SearchHandler implements HttpHandler {
	
	public void handle(HttpExchange exchange) throws IOException {
		
		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		
		InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), "utf-8");
        BufferedReader br = new BufferedReader(isr);
        String request = br.readLine();
        
        // Parse JSON request
        JSONParser parser = new JSONParser();
    	JSONObject jsonObject = null;
    	
		try {
			jsonObject = (JSONObject) parser.parse(request);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String query = (String)jsonObject.get("query");
		String sessionID = (String)jsonObject.get("sessionID");
		
		System.out.println("New Request : " + query);
		
		// Process the query
		long queryStart = System.nanoTime();
		
		String queryResponse = "";
		
		if(ReviewServer.useLucene)
		    queryResponse = new IndexerLucene().searchRequest(query, 2);
		else
			queryResponse = new ElasticSearchRequest().searchRequest(query, 2);
		
		double queryTime = ((double)((System.nanoTime()-queryStart)/10000))/100;
		
		System.out.println("New Request : " + query + " -> Response took " + queryTime + " ms");
		
		boolean querySuccess = true;
		
		if(queryResponse.equals("[]"))
			querySuccess = false;
		
		String response = "{\"querySuccessful\":\""+querySuccess+"\",\"response\":"+queryResponse+"}";
		
		// Send response
		exchange.sendResponseHeaders(200, response.length());
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
	}
}
