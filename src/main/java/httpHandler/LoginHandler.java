package httpHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import reviewServer.Database;

public class LoginHandler implements HttpHandler  {

	public void handle(HttpExchange exchange) throws IOException {
		
		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		
		InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), "utf-8");
        BufferedReader br = new BufferedReader(isr);
        String request = br.readLine();
        
        // Parse JSON request
        JSONParser parser = new JSONParser();
    	JSONObject jsonObject = null;
    	
		try {
			jsonObject = (JSONObject) parser.parse(request);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		String username = (String)jsonObject.get("user");
		String password = (String)jsonObject.get("password");
        
        // Login user
		String loginResult = "false";
		String sessionID = "";
		
		Database userDB = new Database();
        boolean loginRes = userDB.loginUser(username, password);
        sessionID = userDB.getSession(username);
        
        if(loginRes == true) {
        	// Login successful
        	loginResult = "true";
        	System.out.println("User '" + username + "' logged in!");
        }
        else
        	System.out.println("Login failed");
        
        // send response
        String response = "{\"loginResult\":\""+loginResult+"\",\"sessionID\":\""+sessionID+"\"}";
        exchange.sendResponseHeaders(200, response.length());
        OutputStream os = exchange.getResponseBody();
        os.write(response.toString().getBytes());
        os.close();
	}
	
	
	
}
