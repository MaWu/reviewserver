package Indexer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Keywords {
	
	static boolean isInit = false;
	// Contains the topic as a key, the terms as a list, e.g cleanliness->cleanness,tidy,hygiene...
	static HashMap <String, List<String>> topicMap;
	// Contains the term as a key, the topic as a value, e.g. smelly->cleanliness
	static HashMap <String, String> termsMap;
	
	public Keywords()
	{
		if(isInit == false)
		{
			if(loadKeywordsFromFile("src\\main\\resources\\aspect_keywords.py") == true)
				isInit = true;
		}
	}
	
	public String getScoreKeyString(String pTopic)
	{
		if(pTopic.equals("cleanliness"))
			return "score_cl";
		else if(pTopic.equals("comfort"))
			return "score_co";
		else if(pTopic.equals("staff"))
			return "score_st";
		else if(pTopic.equals("value_for_money"))
			return "score_vfm";
		else if(pTopic.equals("location"))
			return "score_loc";
		else if(pTopic.equals("wifi"))
			return "score_wi";
		else if(pTopic.equals("facilities"))
			return "score_fac";
		
		return "";
	}
	
	public String getRelatedTopic(String pTerm)
	{
		if(!isInit)
			return pTerm;
		// find the term and get the topic
		String topic = termsMap.get(pTerm);
		if(topic == null || topic.equals(""))
			return pTerm;
		return topic;
	}
	
	public List<String> getRelatedKeywords(String pTerm)
	{
		if(!isInit)
			return null;
		
		// find the term and get the topic
		String topic = termsMap.get(pTerm);
		
		if(topic == null || topic.equals(""))
			return null;
		
		// We found the topic. Now get the topicList
		List<String>relTermsList = topicMap.get(topic);
		
		if(relTermsList == null || relTermsList.size() == 0)
			return null;
		
		return relTermsList;
	}
	
	private boolean loadKeywordsFromFile(String pFilename)
	{
		// Reset HashMaps
		topicMap = new HashMap <String, List<String>>();
		termsMap = new HashMap <String, String>();
		
		try {
			
			String contents = new String(Files.readAllBytes(Paths.get(pFilename)));
			
			// delete 'keywords = '
			contents = contents.replace("keywords = ", "");
			contents = contents.replace("\'", "\"");
			
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = (JSONArray) parser.parse(contents);
			
			// Go through array
			for(Object obj: jsonArray){
				if (obj instanceof JSONObject) ;
				else continue;
				
				JSONObject jsonObj = (JSONObject)obj;
				String topic = (String)jsonObj.get("topic");
				JSONArray termArray = (JSONArray)jsonObj.get("terms");
				List<String>termsList = new ArrayList<String>();
				
				// Iterate terms
				for(Object objTerm: termArray){
					if (objTerm instanceof String) ;
					else continue;
					
					String jsonTerm = (String)objTerm;
					termsList.add(jsonTerm);
					termsMap.put(jsonTerm, topic);
				}
				
				topicMap.put(topic, termsList);
			}
			// Loading successful
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
}
