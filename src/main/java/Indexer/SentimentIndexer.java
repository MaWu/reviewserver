package Indexer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import SentimentAnalysis.POSDependency;
import SentimentAnalysis.SentimentAnalyzer;

public class SentimentIndexer {
	
	private static boolean isInit = false;
	public static HashMap<String, Object> indexedData;
	public static int productCounter = 0;
	
	SentimentAnalyzer sentimentAnalyzer;
	static Keywords keywords;
	
	int queryTopDocs = 2;
	
	public SentimentIndexer()
	{
		if(isInit == false)
		{
			indexedData = new HashMap<String, Object>();
			keywords = new Keywords();
			isInit = true;
		}
	}
	
	public void runTest()
	{
		System.out.println("++++ Run indexer test ++++");
		HashMap<Integer, ProductEntry> products = getProducts("smell", "Rome");
		
		System.out.println("We have " + products.size() + " product results for 'smell' in 'Rome'");
		for (Map.Entry<Integer, ProductEntry> entry : products.entrySet())
		{
			System.out.println("ID: " + entry.getKey() + " -> " + entry.getValue().productName + ", Total score: " + entry.getValue().score +
				              " (Pos " + entry.getValue().scoreP +
					          ", Neg " + entry.getValue().scoreN +
					          ", Undet " + entry.getValue().scoreU + ")");
		}
		
		//searchRequest("smell", 2);
	}
	
	public void startIndexing()
	{
		List <POSDependency> sentimentList;
		/*
		String name = "BestHotel Rome";
		String city = "Rome";
		List<String> reviewList = new ArrayList<String>();
		reviewList.add("The staff were nice and the breakfast options were decent, toast with different spreads, tea and coffee. Hot and cold drinking water available. Bathrooms were clean, showers were powerful and hot. There was washing machines and computers available to use. WiFi was good. There was a really bad smell in the room, like someone dirty had been living in there for too long. It was small a cramped with not enough room for anyone's bags. The mattress was really thin so it was very uncomfortable. It's overpriced compared to other hostels nearby. No comfy lounge area, just wooden seats in the bar. There was a really great smell in the room.");
		reviewList.add("The employees were nice and the breakfast was tasty.");
		reviewList.add("We had a great stay at this hotel! There was a really great smell in the room.");
		indexProduct(name, city, reviewList);
		
		name = "City Hotel";
		city = "Rome";
		reviewList = new ArrayList<String>();
		reviewList.add("Great location close to the action and huge yummy breakfast every day! Liked the owner's ecological responsibility for their environment. Beautiful grounds and house - a pleasure to stay at. The bed was too soft for my own taste and the room slightly smelled of damp and could have done with a good airing/sunshine to dry out. We didn't use the air con at night and had lots of rain so maybe this could have been the issue...");
		indexProduct(name, city, reviewList);
		
		name = "Grand Hotel";
		city = "Rome";
		reviewList = new ArrayList<String>();
		reviewList.add("The staff were nice and the breakfast options were decent, toast with different spreads, tea and coffee. Hot and cold drinking water available. Bathrooms were clean, showers were powerful and hot. There was washing machines and computers available to use. WiFi was good. There was a really bad smell in the room, like someone dirty had been living in there for too long. It was small a cramped with not enough room for anyone's bags. The mattress was really thin so it was very uncomfortable. It's overpriced compared to other hostels nearby. No comfy lounge area, just wooden seats in the bar. There was a really great smell in the room.");
		indexProduct(name, city, reviewList);
		*/
		SQLConnector sqlCon = new SQLConnector();
		int maxHotels = 500;
		
		for(int i=1;i<=maxHotels;i++)
		{
			ResultSet res = sqlCon.getReviewsFromDB(i);
			String city = "none";
			String name = "none";
			List<String> reviewList = new ArrayList<String>();
			
			try {
				while(res.next())
				{
					// Add hotel database entry to indexer
					String rev_plus = res.getString("rev_plus");
					String rev_minus = res.getString("rev_minus");
					reviewList.add(rev_plus + "." + rev_minus);
					name = res.getString("name");
				}
			} catch (SQLException e) { e.printStackTrace(); }
			
			sqlCon.closeCon();
			indexProduct(name, city, reviewList);
			System.out.println("Analyzed hotel " + i + "/" + maxHotels + " - We analyzed " +  reviewList.size() + " Reviews");
		}
		
		
		
	}
	
	/*
	 * Adds a product to the indexer
	 */
	
	@SuppressWarnings("unchecked")
	public void indexProduct(String pName, String pCity, List<String> pReviewList)
	{
		if(sentimentAnalyzer == null)
			sentimentAnalyzer = new SentimentAnalyzer();
		
		// Create the product
		int reviewID = 0;
		
		List <ProductEntry> entriesToFinish = new ArrayList<ProductEntry>();
		
		// For each review
		for(String review : pReviewList)
		{
			reviewID++;
			List<POSDependency> sentimentList = sentimentAnalyzer.analyzeReview(review);
			
			// Add the sentimentList to our indexer
			for(POSDependency senti : sentimentList)
			{
				String topic = keywords.getRelatedTopic(senti.dependent);
				
				// If sentiment does not exist, add a new HashMap containing the List of Products
				if(!indexedData.containsKey(topic))
					indexedData.put(topic, new HashMap<String, Object>());
				
				// Get the HashMap for the sentiment
				HashMap<String, HashMap<Integer, ProductEntry>> senitHashMap = (HashMap<String, HashMap<Integer, ProductEntry>>) indexedData.get(topic);
				
				// If the location does not exist, add the location and a new HashMap 
				if(!senitHashMap.containsKey(pCity))
					senitHashMap.put(pCity, new HashMap<Integer, ProductEntry>());
				
				ProductEntry p = new ProductEntry(productCounter, pName, topic, pReviewList.size());
				
				// If the product does not exist yet, create it 
				if(!senitHashMap.get(pCity).containsKey(p.id))
					senitHashMap.get(pCity).put(p.id, p);
				
				// Add the new sentiment from this review
				senitHashMap.get(pCity).get(p.id).addSentiment(reviewID, senti.score);
				
				entriesToFinish.add(senitHashMap.get(pCity).get(p.id));
			}
			
			// Add the general score of this review to all other sentiments that have not been mentioned
			// todo
		}
		
		// Finish Products and calculate the scores
		for(ProductEntry prod : entriesToFinish)
		{
			prod.calculateScore();
		}
		
		// Increment product counter
		productCounter++;
	}
	
	/*
	 * Get the hash map for a specific sentiment and a location. For example 'staff' and 'Rome' returns all hotels
	 * that have an entry for 'staff' and are located in 'Rome'.
	 * The hash map contains the productIDs and the ProductEntrys for all products that fit the parameters
	 */
	
	@SuppressWarnings("unchecked")
	public HashMap<Integer, ProductEntry> getProducts(String pSentiment, String pLocation)
	{
		HashMap<String, Object> sentiMap = (HashMap<String, Object>) indexedData.get(pSentiment);
		
		if(sentiMap == null)
			return new HashMap<Integer, ProductEntry>();
		
		HashMap<Integer, ProductEntry> resultMap = (HashMap<Integer, ProductEntry>) sentiMap.get(pLocation);
		
		// Return an empty hash map, if nothing was found that fits the search query
		if(resultMap == null)
			return new HashMap<Integer, ProductEntry>();
		
		return resultMap;
	}
	
	/*
	public HashMap<Integer, ProductEntry> getProductsRelated(String pSentiment, String pLocation)
	{
		List<String> relKeywords = keywords.getRelatedKeywords(pSentiment);
		
		// No keywords found, just execute the query
		if(relKeywords == null)
		{
			System.out.println("No keywords found for " + pSentiment);
			return getProducts(pSentiment, pLocation);
		}
			
		
		HashMap<Integer, ProductEntry> sentiMapResults = new HashMap<Integer, ProductEntry>();
		
		for(String keyword : relKeywords)
		{
			sentiMapResults.putAll(getProducts(keyword, pLocation));
			System.out.println("Check keyword " + keyword + " , newSize " + sentiMapResults.size());
		}
		
		return sentiMapResults;
	}*/
	
	/*
	 * This is the query.
	 * It returns the JSON formatted string with all products that match the query
	 */
	
	@SuppressWarnings("unchecked")
	public String searchRequest(String pQuery, int pTopDocs)
	{
		int docCounter = 0;
		HashMap<Integer, ProductEntry> result = getProducts(keywords.getRelatedTopic(pQuery), "none");
		
		// No documents found
		if(result.size() == 0)
		{
			System.out.println("Query failed! No results");
			return "[]";
		}
		JSONArray responseArray = new JSONArray();
		
		// Add the top documents to the result
		for (Map.Entry<Integer, ProductEntry> entry : result.entrySet())
		{
			if(++docCounter >= pTopDocs)
				break;
			int undet = entry.getValue().totalReviewCounter - entry.getValue().reviewsPositive - entry.getValue().reviewsNegative;
			responseArray.add(entry.getValue().toJson());
			System.out.println("Pcount " + entry.getValue().reviewsPositive + " Ncount " + entry.getValue().reviewsNegative + " Ucount " + undet +
					           "; Pscore " + (int)entry.getValue().scoreP + ", Nscore " + (int)entry.getValue().scoreN + ", Uscore " + (int)entry.getValue().scoreU);
		}
		
		return responseArray.toString();
	}
	
	public void save()
	{
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("sentimentsSaved")))
		{
			// Save lastData to file
			oos.writeObject(productCounter);
			oos.writeObject((HashMap<String, Object>) indexedData);
			oos.close();
			System.out.println("Saved '" + productCounter + "' products with '" + indexedData.size() + "' distinct sentiments to disk");
		}
		catch(Exception e) { System.out.println("Could not save 'sentimentsSaved'"); e.printStackTrace(); }
	}
	
	@SuppressWarnings("unchecked")
	public boolean load()
	{
		
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("sentimentsSaved")))
		{
			SentimentIndexer si = new SentimentIndexer();
			productCounter = (int)ois.readObject();
			indexedData = (HashMap<String, Object>)ois.readObject();
			ois.close();
			System.out.println("Loaded '" + productCounter + "' products with '" + indexedData.size() + "' distinct sentiments from disk");
			return true;
		} catch (FileNotFoundException ex) {
		    System.out.println("Could not find file 'sentimentsSaved'");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
}


