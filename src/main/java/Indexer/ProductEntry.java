package Indexer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

public class ProductEntry implements Serializable {

	private static final long serialVersionUID = 1L;
	
	int id;
	String productName;  // here: hotel name
	String topic;
	int reviewsPositive = 0;
	int reviewsNegative = 0;
	// How many reviews do exist for this product
	int totalReviewCounter = 0;
	int lastReviewID = 0;
	
	double score = 0;
	double scoreP = 0;
	double scoreN = 0;
	double scoreU = 0;
	
	// Contains all scores, but not the last one
	List <Double> scoreListP;
	List <Double> scoreListN;
	List <Double> scoreListU;
	
	// Contains only the scores from the last review added
	List <Double> scoreBuffer;
	
	public ProductEntry(int pID, String pName, String pTopic, int pTotalReviewCounter)
	{
		id = pID;
		productName = pName;
		topic = pTopic;
		reviewsPositive = 0;
		reviewsNegative = 0;
		totalReviewCounter = pTotalReviewCounter;
		
		scoreListP = new ArrayList<Double>();
		scoreListN = new ArrayList<Double>();
		scoreListU = new ArrayList<Double>();
		
		scoreBuffer = new ArrayList<Double>();
	}
	
	public void addSentiment(int pReviewID, double pScore)
	{
		if(scoreBuffer == null)
		{
			System.out.println("Error. This product with id '" + id +"' already has been finished and its finals score has been calculated. Can not add a new sentiment.");
			return;
		}
		
		// This is the first sentiment from the current review
		if(lastReviewID < pReviewID)
			finishLastReview();
		
		// Add to score buffer
		scoreBuffer.add(pScore);
		lastReviewID = pReviewID;
	}
	
	/*
	 * Calculates the score for the last review
	 */
	
	public void finishLastReview()
	{
		if(scoreBuffer == null || scoreBuffer.size() == 0)
			return;
		
		double scoreTotalBuffer = 0;
		// Calculate the average score from the last review and add it to the total score list
		for(double p1 : scoreBuffer)
		{
			scoreTotalBuffer += p1;
		}
		
		scoreTotalBuffer /= scoreBuffer.size();
		
		if(scoreTotalBuffer >= 0)
		{
			scoreListP.add(scoreTotalBuffer);
			reviewsPositive++;
		}
		else
		{
			scoreListN.add(scoreTotalBuffer);
			reviewsNegative++;
		}
		
		scoreBuffer = new ArrayList<Double>();
		//System.out.println("Score added for '" + productName + "' with score " + scoreTotalBuffer);
	}
	
	/*
	 * Calculates the final score for this product and sentiment
	 */
	
	public void calculateScore()
	{
		finishLastReview();
		
		if(scoreListP == null && scoreListN == null)
			return;
		
		double scoreP = 0;
		double scoreN = 0;
		double scoreU = 0.1f;
		double probP = (double)reviewsPositive/(double)totalReviewCounter;
		double probN = (double)reviewsNegative/(double)totalReviewCounter;
		double probU = (double)(totalReviewCounter-reviewsPositive-reviewsNegative)/(double)totalReviewCounter;
		
		// Calculate the positive score
		for(double p1 : scoreListP)
			scoreP += p1;
		
		scoreP /= reviewsPositive;
		
		// Calculate the negative score
		for(double p1 : scoreListN)
			scoreN += p1;
		
		scoreN /= reviewsNegative;
		
		// Calculate the negative score
		for(double p1 : scoreListU)
			scoreU += p1;
		
		scoreU /= (totalReviewCounter-reviewsPositive-reviewsNegative);
		
		if(Double.isNaN(scoreP)) scoreP = 0;
		if(Double.isNaN(scoreN)) scoreN = 0;
		if(Double.isNaN(scoreU) || Double.isInfinite(scoreU)) scoreU = 0;
		
		score = probP * scoreP + probN * scoreN + scoreU * probU;
		
		this.scoreP = scoreP*100;
		this.scoreN = scoreN*100;
		this.scoreU = scoreU*100;

		if(this.scoreN < 0) this.scoreN*=-1;
		if(this.scoreU < 0) this.scoreU*=-1;
		
		scoreBuffer = null;
		/*
		System.out.println("Total score for Hotel " + productName + " and topic " + topic +" is " + score);
		System.out.println("Pos " + reviewsPositive + "->" + scoreP +
				         ", Neg " + reviewsNegative + "->" + scoreN +
				         ", Un " + (totalReviewCounter-reviewsPositive-reviewsNegative) + "->" + scoreU);*/
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJson()
	{
		JSONObject object = new JSONObject();
		object.put("name", productName);
		object.put("location", "testLocation");
		
		int posValue = (int)scoreP;
		int negValue = (int)scoreN;
		int uncerValue = (int)scoreU;
		
		// Add scores to display
		object.put("score", "[{" +
		                    "\"type\":\"Positive\"," +
		                    "\"value\":"+posValue+
		                    "},{" +
		                    "\"type\":\"Negative\"," +
		                    "\"value\":"+negValue+
		                    "},{" +
		                    "\"type\":\"Uncertain\"," +
		                    "\"value\":"+uncerValue+
		                    "}]");
		
		return object;
	}
	
}
