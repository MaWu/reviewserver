package Indexer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLConnector {

	Connection con = null;
	
	String sqlDriver = "org.gjt.mm.mysql.Driver";
	String serverURL = "jdbc:mysql://localhost?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Berlin";
	
	public ResultSet getReviewsFromDB()
	{
		try
		{
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			
			con = DriverManager.getConnection(serverURL, "reviewDBuser", "MostSecurePaswrdis12345");
			
			Statement st = con.createStatement();
			int limit = 100000;
			ResultSet rs = st.executeQuery("SELECT r_id, score, rev_plus, rev_minus, h_id FROM bcom_dataset.review LIMIT 100000");
			return rs;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ResultSet getReviewsFromDB(int pHotelID)
	{
		try
		{
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			
			con = DriverManager.getConnection(serverURL, "reviewDBuser", "MostSecurePaswrdis12345");
			
			int limit = 100000;
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT t1.r_id, t1.score, t1.rev_plus, t1.rev_minus, t2.name, t1.h_id FROM bcom_dataset.review t1 INNER JOIN bcom_dataset.hotel t2 ON t1.h_id = t2.h_id and t1.h_id="+pHotelID+" LIMIT "+limit);
			
			//ResultSet rs = st.executeQuery("SELECT t1.r_id, t1.score, t1.rev_plus, t1.rev_minus, t2.name FROM bcom_dataset.review t1 INNER JOIN bcom_dataset.hotel t2 ON t1.h_id = t2.h_id LIMIT 500");
			return rs;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ResultSet getHotelsFromDB()
	{
		try
		{
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			
			con = DriverManager.getConnection(serverURL, "reviewDBuser", "MostSecurePaswrdis12345");
			
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT h_id, name, address, description, score, score_cl, score_co, score_loc, score_fac, score_st, score_vfm, score_wi FROM bcom_dataset.hotel");
			return rs;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void closeCon()
	{
		try {
			con.close();
		} catch (Exception e) { }
	}
	
}