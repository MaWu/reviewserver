package Indexer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.ByteBuffersDirectory;
import org.apache.lucene.store.Directory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import SentimentAnalysis.POSDependency;
import SentimentAnalysis.SentimentAnalyzer;

@SuppressWarnings("unchecked")
public class IndexerMain {
	
	public static Directory memoryIndex = new ByteBuffersDirectory();
	private static boolean isInit = false;
	
	int queryTopDocs = 2;
	
	public IndexerMain()
	{
		if(isInit == false)
		{
			memoryIndex = new ByteBuffersDirectory();
			try { initIndexer(); } catch (IOException e) { System.out.println("Error initIndexer"); }
		}
	}
	
	private void initIndexer() throws IOException
	{
		StandardAnalyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		IndexWriter writter = new IndexWriter(memoryIndex, indexWriterConfig);
		SentimentAnalyzer sentimentAnalyzer = new SentimentAnalyzer();
		List <POSDependency> opinionList;
		
		Document document = new Document();
		document.add(new TextField("name", "BestHotel Rom", Store.YES));
		document.add(new TextField("location", "Rom", Store.YES));
		String review = "The staff were nice and the breakfast options were decent, toast with different spreads, tea and coffee. Hot and cold drinking water available. Bathrooms were clean, showers were powerful and hot. There was washing machines and computers available to use. WiFi was good. There was a really bad smell in the room, like someone dirty had been living in there for too long. It was small a cramped with not enough room for anyone's bags. The mattress was really thin so it was very uncomfortable. It's overpriced compared to other hostels nearby. No comfy lounge area, just wooden seats in the bar.";
		opinionList = sentimentAnalyzer.analyzeReview(review);
		for(POSDependency dep : opinionList)
		{
			document.add(new TextField("review", dep.storeIndexer(), Store.YES));
		}
		writter.addDocument(document);
		
		document = new Document();
		document.add(new TextField("name", "City Hotel", Store.YES));
		document.add(new TextField("location", "Rom", Store.YES));
		review = "Great location close to the action and huge yummy breakfast every day! Liked the owner's ecological responsibility for their environment. Beautiful grounds and house - a pleasure to stay at. The bed was too soft for my own taste and the room slightly smelled of damp and could have done with a good airing/sunshine to dry out. We didn't use the air con at night and had lots of rain so maybe this could have been the issue...";
		opinionList = sentimentAnalyzer.analyzeReview(review);
		for(POSDependency dep : opinionList)
		{
			document.add(new TextField("review", dep.storeIndexer(), Store.YES));
		}
		writter.addDocument(document);
		
		document = new Document();
		document.add(new TextField("name", "City Hotel", Store.YES));
		document.add(new TextField("location", "Rom", Store.YES));
		//document.add(new TextField("review", "Great rooms, perfect Hotel", Store.YES));
		writter.addDocument(document);
		
		writter.close();
		
		isInit = true;
	}
	
	public List<Document> searchIndex(String inField, String queryString) throws IOException, ParseException {
	    
		StandardAnalyzer analyzer = new StandardAnalyzer();
		Query query = new QueryParser(inField, analyzer).parse(queryString);
		
	    IndexReader indexReader = DirectoryReader.open(memoryIndex);
	    IndexSearcher searcher = new IndexSearcher(indexReader);
	    TopDocs topDocs = searcher.search(query, queryTopDocs);
	    List<Document> documents = new ArrayList<Document>();
	    for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
	        documents.add(searcher.doc(scoreDoc.doc));
	    }
	    
	    return documents;
	}
	
	public JSONObject documentToJson(Document pDoc, String pQuery)
	{
		JSONObject object = new JSONObject();
		object.put("name", pDoc.get("name"));
		object.put("location", pDoc.get("location"));
		String[]sentiments = pDoc.getValues("review");
		
		int posValue = 0;
		int negValue = 0;
		int uncerValue = 0;
		
		int posCounter = 0;
		int negCounter = 0;
		
		// Search fields of this Hotel
		for (String sentiment : sentiments)
		{
			String []split = sentiment.split("(;)|(:)");
			
			if(!split[0].toLowerCase().contains(pQuery.toLowerCase()))
				continue;
			
			double score = Double.valueOf(split[2]);
			System.out.println(pDoc.get("name") + " : " + split[0] +" => " + split[1] + ", score " + score);
			if(score >= 0)
			{
				posValue += (int)(score*100);
				posCounter++;
			}
			if(score <= 0)
			{
				negValue += (int)(score*-100);
				negCounter++;
			}
		}
		
		// Calculate avrg positive and negativ Scores
		posValue = (int)((double)posValue / (double)posCounter);
		negValue = (int)((double)negValue / (double)negCounter);
		uncerValue = (100 - posValue - negValue);
		
		// Add scores to display
		object.put("score", "[{" +
		                    "\"type\":\"Positive\"," +
		                    "\"value\":"+posValue+
		                    "},{" +
		                    "\"type\":\"Negative\"," +
		                    "\"value\":"+negValue+
		                    "},{" +
		                    "\"type\":\"Uncertain\"," +
		                    "\"value\":"+uncerValue+
		                    "}]");
		
		return object;
	}
	
	public String searchRequest(String pQuery)
	{
		System.out.println("New Request : " + pQuery);
		
		List<Document> documents = null;
		
		try {
			documents = searchIndex("review", pQuery);
		} catch (Exception e) { System.out.println("Error searchIndex"); e.printStackTrace(); }
		
		// No documents found
		if(documents.size() == 0)
		{
			System.out.println("Query failed! No results");
			return "[]";
		}
		
		JSONArray responseArray = new JSONArray();
		
		for(Document d : documents)
		{
			responseArray.add(documentToJson(d, pQuery));
		}
		
		System.out.println("Query result: "+ responseArray.toString());
		return responseArray.toString();
	}
	
}
