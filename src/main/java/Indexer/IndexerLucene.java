package Indexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class IndexerLucene {
	
	public static FSDirectory memoryIndexHotel;
	public static FSDirectory memoryIndexReviews;
	private static boolean isInit = false;
	
	private boolean doInitFromSQLServer = false;
	
	static Keywords keywords;
	
	static private IndexSearcher searcherReview;
	static private IndexSearcher searcherHotel;
	
	public IndexerLucene()
	{
		if(isInit == false)
		{
			keywords = new Keywords();
			try {
				System.out.println("Loading lucene index from disk");
				memoryIndexHotel = FSDirectory.open(Paths.get("index.luceneA"));
				memoryIndexReviews = FSDirectory.open(Paths.get("index.luceneB"));
				
				IndexReader indexReaderReview = DirectoryReader.open(memoryIndexReviews);
				searcherReview = new IndexSearcher(indexReaderReview);
				
				IndexReader indexReader = DirectoryReader.open(memoryIndexHotel);
				searcherHotel = new IndexSearcher(indexReader);
				
				// Test
				List <String> testList = new ArrayList<String>();
				testList.add("clean");
				getReviewsMatchAny("rev_plus", testList);
				
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			isInit = true;
		}
	}
	
	public void runTest()
	{
		System.out.println("++++ Run indexer test ++++");
		searchRequest("staff", 2);
	}
	
	public void startIndexing()
	{
		if(!doInitFromSQLServer)
			return;
		
		System.out.println("Start lucene indexing");
		try { initIndexer(); } catch (IOException e) { System.out.println("Error initIndexer"); }
		catch (SQLException e) { System.out.println("SQLException"); e.printStackTrace(); }
	}
	
	private void initIndexer() throws IOException, SQLException
	{
		int maxHotels = 500;
		StandardAnalyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		IndexWriter writter = new IndexWriter(memoryIndexHotel, indexWriterConfig);
		
		StandardAnalyzer analyzerReviews = new StandardAnalyzer();
		IndexWriterConfig indexWriterConfigReviews = new IndexWriterConfig(analyzerReviews);
		IndexWriter writterReviews = new IndexWriter(memoryIndexReviews, indexWriterConfigReviews);
		SQLConnector sqlCon = new SQLConnector();
		
		// Get all reviews and index them
		ResultSet rsHotel = sqlCon.getHotelsFromDB();
		
		if(rsHotel == null)
		{
			writter.close();
			return;
		}
		
		while(rsHotel.next())
		{
			// Add hotel database entry to indexer
			String hotelID = rsHotel.getString("h_id");
			String name = rsHotel.getString("name");
			String address = rsHotel.getString("address");
			String desc = rsHotel.getString("description");
			String overallScore = rsHotel.getString("score");
			String score_clean = rsHotel.getString("score_cl");
			String score_comf = rsHotel.getString("score_co");
			String score_location = rsHotel.getString("score_loc");
			String score_facility = rsHotel.getString("score_fac");
			String score_staff = rsHotel.getString("score_st");
			String score_vfm = rsHotel.getString("score_vfm");
			String score_wifi = rsHotel.getString("score_wi");
			
			if(Integer.valueOf(hotelID) > maxHotels)
				continue;
			
			ResultSet rsReviews = sqlCon.getReviewsFromDB(Integer.valueOf(hotelID));
			
			if(rsReviews == null)
			{
				writterReviews.close();
				continue;
			}
			
			int rowCounter = 0;
			
			// Add all reviews for this hotel to the database
			while(rsReviews.next())
			{
				String hotelID2 = rsReviews.getString("h_id");
				String reviewID = rsReviews.getString("r_id");
				String reviewPositive = rsReviews.getString("rev_plus");
				String reviewNegative = rsReviews.getString("rev_minus");
				String overallScoreRev = rsReviews.getString("score");
				
				if(reviewPositive == null)
					reviewPositive = "";
				if(reviewNegative == null)
					reviewNegative = "";
				
				Document document = new Document();
				document.add(new TextField("h_idRev", hotelID2, Store.YES));
				document.add(new TextField("r_id", reviewID, Store.YES));
				document.add(new TextField(/*"h_id="+hotelID+".*/"rev_plus", reviewPositive, Store.YES));
				document.add(new TextField(/*"h_id="+hotelID+".*/"rev_minus", reviewNegative, Store.YES));
				
				document.add(new TextField("scoreRev", overallScoreRev, Store.YES));
				document.add(new SortedDocValuesField("scoreRev", new BytesRef(overallScore)));
				
				writterReviews.addDocument(document);
				rowCounter++;
			}
			
			Document document = new Document();
			document.add(new TextField("h_id", hotelID, Store.YES));
			document.add(new TextField("h_name", name, Store.YES));
			document.add(new TextField("address", address, Store.YES));
			document.add(new TextField("desc", desc, Store.YES));
			
			document.add(new TextField("score", overallScore, Store.YES));
			document.add(new SortedDocValuesField("score", new BytesRef(overallScore)));
			
			document.add(new TextField("score_cl", score_clean, Store.YES));
			document.add(new SortedDocValuesField("score_cl", new BytesRef(score_clean)));
			
			document.add(new TextField("score_co", score_comf, Store.YES));
			document.add(new SortedDocValuesField("score_co", new BytesRef(score_comf)));
			
			document.add(new TextField("score_loc", score_location, Store.YES));
			document.add(new SortedDocValuesField("score_loc", new BytesRef(score_location)));
			
			document.add(new TextField("score_fac", score_facility, Store.YES));
			document.add(new SortedDocValuesField("score_fac", new BytesRef(score_facility)));
			
			document.add(new TextField("score_st", score_staff, Store.YES));
			document.add(new SortedDocValuesField("score_st", new BytesRef(score_staff)));
			
			document.add(new TextField("score_vfm", score_vfm, Store.YES));
			document.add(new SortedDocValuesField("score_vfm", new BytesRef(score_vfm)));
			
			document.add(new TextField("score_wi", score_wifi, Store.YES));
			document.add(new SortedDocValuesField("score_wi", new BytesRef(score_wifi)));
			
			writter.addDocument(document);
			
			//System.out.println("added " + reviewPositive);
			System.out.println("Analyzed hotel " + hotelID + "/" + maxHotels + " - We analyzed " +  rowCounter + " Reviews");
		}
		
		
		
		writter.close();
		writterReviews.close();
		sqlCon.closeCon();
		
		isInit = true;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject documentToJson(Document pDoc, String pQuery)
	{
		JSONObject object = new JSONObject();
		object.put("name", pDoc.get("h_name"));
		object.put("location", pDoc.get("none"));
		
		// Calculate avrg positive and negativ Scores
		int posValue = 100;
		int negValue = 0;
		int uncerValue = (100 - posValue - negValue);
		
		// Add scores to display
		object.put("score", "[{" +
		                    "\"type\":\"Positive\"," +
		                    "\"value\":"+posValue+
		                    "},{" +
		                    "\"type\":\"Negative\"," +
		                    "\"value\":"+negValue+
		                    "},{" +
		                    "\"type\":\"Uncertain\"," +
		                    "\"value\":"+uncerValue+
		                    "}]");
		
		return object;
	}
	
	public String searchRequest(String pQuery, int pTopDocs)
	{
		long queryStart = System.nanoTime();
		
		String topic = keywords.getRelatedTopic(pQuery);
		List <String> aspectsKeywords = keywords.getRelatedKeywords(topic);
	    String scoreString = keywords.getScoreKeyString(topic);
		List<Document> documents = new ArrayList<Document>();
		
		if(aspectsKeywords == null)
		{
			aspectsKeywords = new ArrayList<String>();
			aspectsKeywords.add(pQuery);
		}
		
		// The maps contain how many positive/neg/und reviews each hotel (with the hotelID) has
		// HotelID is mapped -> to number of pos/neg/und reviews
		HashMap <String, Integer> counterP = new HashMap<String, Integer>();
		HashMap <String, Integer> counterN = new HashMap<String, Integer>();
		HashMap <String, Integer> counterU = new HashMap<String, Integer>();
		
		// Get Positive
		TopDocs topDocsP = getReviewsMatchAny("rev_plus", aspectsKeywords);
		double time1 = ((double)((System.nanoTime()-queryStart)/10000))/100;
		counterP = createCountMap(topDocsP);
		double time2 = ((double)((System.nanoTime()-queryStart)/10000))/100-time1;
		
		System.out.println("Found '" + topDocsP.scoreDocs.length + "' reviews about topic '" + topic + "' in positive review part.");
		System.out.println("Requesting positive reviews took " + time1 + " ms");
		System.out.println("Counting positive reviews took " + time2 + " ms");
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
		
		// Get Negatives
		TopDocs topDocsN = getReviewsMatchAny("rev_minus", aspectsKeywords);
		double time3 = ((double)((System.nanoTime()-queryStart)/10000))/100-time1-time2;
		counterN = createCountMap(topDocsN);
		double time4 = ((double)((System.nanoTime()-queryStart)/10000))/100-time1-time2-time3;
		
		System.out.println("Found '" + topDocsN.scoreDocs.length + "' reviews about topic '" + topic + "' in negative review part.");
		System.out.println("Requesting negative reviews took " + time3 + " ms");
		System.out.println("Counting negative reviews took " + time4 + " ms");
		
		// Get reviews not mentioning any keyword
		/*
		List <String> keyFields = new ArrayList<String>(); keyFields.add("rev_plus"); keyFields.add("rev_minus");
		TopDocs topDocsU = getReviewsMatchNone(keyFields, aspectsKeywords);
		double time5 = ((double)((System.nanoTime()-queryStart)/10000))/100-time1-time2-time3-time4;
		counterU = createCountMap(topDocsP);
		double time6 = ((double)((System.nanoTime()-queryStart)/10000))/100-time1-time2-time3-time4-time5;
		*/
		
		/*
		System.out.println("Found '" + topDocsU.scoreDocs.length + "' reviews not containing topic '" + topic + "' in any review part.");
		System.out.println("Requesting negative reviews took " + time5 + " ms");
		System.out.println("Counting negative reviews took " + time6 + " ms");*/
		
		
		// Only return top docs
		if(documents.size()>2)
		{
			Document d1 = documents.get(0);
			Document d2 = documents.get(1);
			documents = new ArrayList<Document>();
			documents.add(d1);
			documents.add(d2);
		}
		
		// No documents found
		if(documents.size() == 0)
		{
			System.out.println("Query failed! No results");
			return "[]";
		}
		
		JSONArray responseArray = new JSONArray();
		
		for(Document d : documents)
		{
			responseArray.add(documentToJson(d, pQuery));
		}
		
		System.out.println("Query result: "+ responseArray.toString());
		return responseArray.toString();
	}
	
	private TopDocs getReviewsMatchAny(String pKey, List <String> pAspectsKeywords)
	{
		BooleanQuery.Builder boolQuery = new BooleanQuery.Builder();
	    boolQuery.setMinimumNumberShouldMatch(1);
	    for(String aspect : pAspectsKeywords)
		{
			TermQuery termQuery = new TermQuery(new Term(pKey, aspect));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
		}
	    
	    try {
			return searcherReview.search(boolQuery.build(), 500000);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private TopDocs getReviewsMatchNone(List <String> pKey, List <String> pAspectsKeywords)
	{
		BooleanQuery.Builder boolQuery = new BooleanQuery.Builder();
	    for(String aspect : pAspectsKeywords)
		{
			for(String key : pKey)
			{
				TermQuery termQuery = new TermQuery(new Term(key, aspect));
				boolQuery.add(termQuery, BooleanClause.Occur.MUST_NOT);
			}
			TermQuery termQuery = new TermQuery(new Term("rev_plus", "is"));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
			termQuery = new TermQuery(new Term("rev_minus", "is"));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
			termQuery = new TermQuery(new Term("rev_plus", "the"));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
			termQuery = new TermQuery(new Term("rev_minus", "the"));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
			termQuery = new TermQuery(new Term("rev_plus", "a"));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
			termQuery = new TermQuery(new Term("rev_minus", "a"));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
			termQuery = new TermQuery(new Term("rev_plus", "in"));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
			termQuery = new TermQuery(new Term("rev_minus", "in"));
			boolQuery.add(termQuery, BooleanClause.Occur.SHOULD);
		}
		
		try {
			return searcherReview.search(boolQuery.build(), 500000);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * We create a HashMap that contains, which document
	 */
	
	private HashMap<String, Integer> createCountMap(TopDocs pDocs)
	{
		HashMap <String, Integer> counterBuf = new HashMap<String, Integer>();
		
		for (ScoreDoc scoreDoc : pDocs.scoreDocs) {
			try {
				Document d = searcherReview.doc(scoreDoc.doc);
				String hotelID = d.get("h_idRev");
				counterBuf.put(hotelID, counterBuf.containsKey(hotelID) ? (counterBuf.get(hotelID)+1) : 1);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("We have found " + pDocs.scoreDocs.length +" reviews for " + counterBuf.size() + " hotels");
		
		return counterBuf;
	}
	
	// For compatability
	public boolean load()
	{
		return true;
	}
	public boolean save()
	{
		return true;
	}
}
