package SentimentAnalysis;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.GrammaticalStructure;

public class SentimentAnalyzer {
	
	static boolean isInit = false;
	
	static List <POSDependency> opinionList;
	
	public static void setOpinionList(List<POSDependency> opinionList) {
		SentimentAnalyzer.opinionList = opinionList;
	}

	public static List<POSDependency> getOpinionList() {
		return opinionList;
	}
	
	static MaxentTagger tagger;
	static DependencyParser parser;
	
	public SentimentAnalyzer()
	{
		if(isInit == false)
			initAnalyzer();
	}
	
	public void initAnalyzer() {
		
		System.out.println("Please wait a moment. Error message does not matter.");
		System.out.println("----------------------------------------------------");
		
		String modelPath = DependencyParser.DEFAULT_MODEL;
	    String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
	    
	    tagger = new MaxentTagger(taggerPath);
	    parser = DependencyParser.loadFromModelFile(modelPath);
	    
	    isInit = true;
	}
	public List <POSDependency> analyzeReview(String pReview)
	{
		if(isInit == false)
			initAnalyzer();
		
		DocumentPreprocessor tokenizer = new DocumentPreprocessor(new StringReader(pReview));
	    
	    opinionList = new ArrayList<POSDependency>();
	    
	    for (List<HasWord> sentence : tokenizer) {
	      List<TaggedWord> tagged = tagger.tagSentence(sentence);
	      GrammaticalStructure gs = parser.predict(tagged);
	      
	      Object gsA = gs.allTypedDependencies().toArray();
	      List <POSDependency> dependencyList = new ArrayList<POSDependency>();
	      
	      // Fill Dependencies
	      for(Object entry : gs.allTypedDependencies().toArray())
	      {
	    	  POSDependency dependency = new POSDependency(entry.toString(), tagged);
	    	  if(dependency.governor.isEmpty() || dependency.dependent.isEmpty())
	    		  continue;
	    	  dependencyList.add(dependency);
	      }
	      
	      for(POSDependency dependency : dependencyList)
	      {
	    	  if(dependency == null || dependency.relationType == null)
	    		  continue;
	    	  
	    	  // Nominal Subject
	    	  if(dependency.relationType.equals("nsubj"))
	    	  {
	    		  if(dependency.dependentTag.equals("JJ") && dependency.governorTag.contains("NN"))
	    		  {
	    			  opinionList.add(dependency.swap());
	    		  }
	    	  }
	    	  
	    	  // When relation is 'amod' and Gov is an adjective
	    	  if(dependency.relationType.equals("amod"))
	    	  {
	    		  if(dependency.dependentTag.equals("NN") && dependency.governorTag.equals("JJ"))
	    			  opinionList.add(dependency);
	    	  }
	    	  
	    	  // Noun two words long
	    	  if(dependency.relationType.equals("nsubj"))
	    	  {
	    		  POSDependency depOther = containsConnectionDep(dependencyList, "compound", dependency.governorIndex);
	    		  if(depOther != null)
	    		  {
	    			  POSDependency newOpinion = new POSDependency(depOther.governor + " " + depOther.dependent, "NN", dependency.dependent, dependency.dependentTag);
	    			  opinionList.add(newOpinion);
	    		  }
	    	  }
	    	  
	    	  // Dep is an adjective, but Gov is not a noun. Find what Gov referres to
	    	  if(dependency.relationType.equals("nsubj") &&
	    	     dependency.dependentTag.equals("JJ") &&
	    	     !dependency.governorTag.equals("NN"))
	    	  {
	    		  
	    	  }
	    	  
	    	  // 
	    	  if(dependency.relationType.equals("nsubj") &&
	    	     dependency.dependentTag.contains("VB"))
	    	  {
	    		  POSDependency depOther = containsConnectionDep(dependencyList, "dobj", dependency.dependentIndex);
	    		  if(depOther != null)
	    		  {
	    			  POSDependency newOpinion = depOther.swap();
	    			  opinionList.add(newOpinion);
	    		  }
	    	  }
	    	  
	    	  // Adjectival Complement
	    	  if(dependency.relationType.equals("nsubj") &&
	    	     dependency.dependentTag.contains("VB"))
	    	  {
	    		  POSDependency depOther = containsConnectionDep(dependencyList, "advmod", dependency.dependentIndex);
	    		  //System.out.println("found " + depOther.toString());
	    		  if(depOther != null && depOther.governorTag.equals("JJ"))
	    		  {
	    			  POSDependency newOpinion = new POSDependency(dependency.swap(), depOther);
	    			  opinionList.add(newOpinion);
	    		  }
	    	  }
	    	  
	    	  // More to add...
	      }
	    }
	    
	    // Calculate scores
	    calculateScores();
	    
	    // Print opinions
	    //System.out.println("Opinions extracted from test-review");
	    //printOpinions();
	    
	    
	    return opinionList;
	}
	
	private void calculateScores()
	{
		SentiWordNet sentiwordnet = new SentiWordNet();
		List <POSDependency> deleteOpinionList = new ArrayList<POSDependency>();
		
		for (POSDependency d : opinionList) {
			
			String type = "a";
	    	
			if(d.governorTag.contains("VB"))
	    		type = "v";
			
			d.score = sentiwordnet.extract(d.governor.toLowerCase(), type);
	    	
	    	// Remove entry for now, if score is too undetermined
	    	if(d.score > -0.1 && d.score < 0.1)
	    		deleteOpinionList.add(d);
	    }
		
		// Now delete all opinions in deleteOpinionList
		for (POSDependency d : deleteOpinionList)
			opinionList.remove(d);
	    
	}
	
	private void printOpinions()
	{
		for (POSDependency d : opinionList) {
			System.out.println(d.dependent + " => " + d.governor + ", score : " + d.score);
	    }
	}
	
	// Searches for a dependent with given Index
	private POSDependency containsConnectionDep(List <POSDependency> pDependencyList, String pRelType, int pIndex)
	{
		for (POSDependency d : pDependencyList) {
			if(d == null || d.relationType == null || !d.relationType.equals(pRelType))
				continue;
			if(d.dependentIndex == pIndex)
				return d;
			}
		return null;
		}
}
