package SentimentAnalysis;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.TaggedWord;

public class POSDependency implements Serializable {
	
	private static final long serialVersionUID = 5128757693255626257L;
	
	public String dependent;
	public String dependentTag;
	int dependentIndex;
	
	public String governor;
	public String governorTag;
	int governorIndex;
	
	public double score = 0;
	
	String relationType;
	
	public POSDependency(String pDep, String pPosDep, String pGov, String pPosGov)
	{
		dependent = pDep;
		dependentTag = pPosDep;
		governor = pGov;
		governorTag = pPosGov;
	}
	
	public POSDependency(POSDependency p1, POSDependency p2)
	{
		dependent = p1.dependent;
		dependentTag = p1.dependentTag;
		dependentIndex = p1.dependentIndex;
		
		governor = p2.governor;
		governorTag = p2.governorTag;
		governorIndex = p2.governorIndex;
	}
	
	public POSDependency(String pPair, List<TaggedWord> pTagged)
	{
		dependent = extractDependent(pPair);
		governor  = extractGovernor (pPair);
		
		String dep = extractDependentIndex(pPair);
		String gov = extractGovernorIndex(pPair);
		
		if(dep.isEmpty() || gov.isEmpty())
			return;
		
		// Get the POS-Tagging from list
		dependentIndex = Integer.valueOf(dep)-1;
		governorIndex = Integer.valueOf(gov)-1;
		
		// Not root
		if(dependentIndex >= 0)
			dependentTag = pTagged.get(dependentIndex).tag();
		else
			dependentTag = "ROOT";
		
		governorTag = pTagged.get(governorIndex).tag();
		
		relationType = extractRelationType(pPair);
	}
	
	public POSDependency swap()
	{
		POSDependency dep = new POSDependency(governor, governorTag, dependent, dependentTag);
		dep.dependentIndex = governorIndex;
		dep.governorIndex = dependentIndex;
		dep.relationType = dep.relationType;
		return dep;
	}
	
	@Override
	public String toString() {
		return "Relation " + relationType +" Dep : " + dependent + ":" + dependentTag + " Gov : " + governor + ":" + governorTag;
	}
	
	public String storeIndexer() {
		return dependent + ";" + governor + ":" + score;
	}
	
	private static String regex(String pString, String pPattern, int pGroup)
	{
		Pattern pattern = Pattern.compile(pPattern);
		Matcher matcher = pattern.matcher(pString);
		if (matcher.find())
		{
			return matcher.group(pGroup);
		}
		return "";
	}
	
	private static String extractRelationType(String pPair)
	{
		return regex(pPair, "(.*?)\\(", 1);
	}
	
	private static String extractDependent(String pPair)
	{
		return regex(pPair, "\\((.*?)-\\d", 1);
	}
	
	private static String extractDependentIndex(String pPair)
	{
		return regex(pPair, "-(\\d.?),", 1);
	}
	
	private static String extractGovernor(String pPair)
	{
		return regex(pPair, "\\s(.*?)-\\d", 1);
	}
	
	private static String extractGovernorIndex(String pPair)
	{
		return regex(pPair, "-(\\d.?)\\)", 1);
	}
}

