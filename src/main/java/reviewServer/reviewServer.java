package reviewServer;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sun.net.httpserver.HttpServer;

import httpHandler.ElasticSearchRequest;
import httpHandler.GetProductsHandler;
import httpHandler.LoginHandler;
import httpHandler.ProductHandler;
import httpHandler.SearchHandler;
import Indexer.IndexerLucene;
import Indexer.IndexerMain;
import Indexer.Keywords;
import Indexer.SQLConnector;
import Indexer.SentimentIndexer;
import SentimentAnalysis.SentimentAnalyzer;

public class ReviewServer {
	
	int port = 4000;
	public static boolean useLucene = false;
	boolean doSentimentAnalysis = false;
	boolean runWebServer = false;
	
	public ReviewServer()
	{
		Keywords k = new Keywords();
		// Start Server
		startServer();
		new ElasticSearchRequest();
	}
	
	public void startServer()
	{
		if(useLucene)
		{
			//SentimentIndexer indexer = new SentimentIndexer();
			IndexerLucene indexer = new IndexerLucene();
			
			// Do the sentiment analysis, when saved data can't or shouldn't be loaded
			if(doSentimentAnalysis || indexer.load() == false)
			{
				// Init Indexer
				indexer.startIndexing();
				//indexer.runTest();
				
				// Now save all the data
				indexer.save();
			}
		}
		
		if(runWebServer)
		{
			try {
				
				HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
				System.out.println("server started at " + port);
				server.createContext("/login", new LoginHandler());
				server.createContext("/search", new SearchHandler());
				server.setExecutor(null);
				server.start();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		new ReviewServer();
	}

}

