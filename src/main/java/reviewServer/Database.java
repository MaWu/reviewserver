package reviewServer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Database {
	
	static boolean isInit = false;
	
	static List <User> userList = new ArrayList<User>();
	static HashMap<String, User> activeSessions = new HashMap<String, User>();
	
	public Database()
	{
		if(!isInit)
		{
			isInit = true;
			
			userList.add(new User("Admin", "12345"));
			userList.add(new User("Test", "123"));
		}
	}
	
	public String getSession(String pUsername)
	{
		User u = getUser(pUsername);
		
		if(u == null)
			return "";
		return u.sessionID;
	}
	
	public boolean loginUser(String pUsername, String pPassword)
	{
		User u = getUser(pUsername);
		
		if(u == null || !u.password.equals(pPassword))
			return false;
		
		// end old session if it exists
		logoutUser(pUsername);
		
		u.sessionID = generateSessionID();
		
		// Add User to active sessions
		activeSessions.put(u.sessionID, u);
		return true;
	}
	
	public void logoutUser(String pUsername)
	{
		User u = getUser(pUsername);
		activeSessions.remove(u.sessionID);
		u.logout();
	}
	
	private String generateSessionID()
	{
		String sessionGenerated = UUID.randomUUID().toString();
		
		// If sessionID already has been given to a user, just generate a new one
		if(activeSessions.containsKey(sessionGenerated))
			return generateSessionID();
		
		return sessionGenerated;
	}
	
	private User getUser(String username)
	{
		for(User u : userList)
		{
			if(u.name.equals(username))
				return u;
		}
		return null;
	}
	
	private class User
	{
		String name;
		String password;
		String sessionID = "";
		
		public User(String pName, String pPassword)
		{
			name = pName;
			password = pPassword;
		}
		
		public void logout()
		{
			sessionID = "";
		}
	}
	
}
