import React from 'react'
import { PieChart } from '@opd/g2plot-react'

const Pie = (props) => {

  const config = {
    forceFit: true,
    title: {
      visible: false,
      text: 'ScorePie',
    },
    description: {
      visible: false,
      text: '',
    },
    data: props.data,
    radius: 0.4,
    angleField: 'value',
    colorField: 'type',
    color: ['green', 'red', 'grey'],
    label: {
      visible: true,
      type: 'inner',
    },
  }

  return (
    <section>
      <PieChart {...config} />
    </section>
  );
}

export default Pie