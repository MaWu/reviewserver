import React from 'react'
import Pie from "./Charts/Pie";
import { Grid, Segment, Header } from 'semantic-ui-react'

const CompareTwoDocuments = props => {

  const result = props.topDocuments.map((element, index) => {
    return (
      <Grid.Column>
        <Segment>{SimpleDocumentElement(element)}</Segment>
      </Grid.Column>
    )
  })

  return result
}

const SimpleDocumentElement = props => {

  return (
    <div>
      <p>Hotel : {props.name}</p>
      <p>Location : {props.location}</p>
      <div id="limitPieChart">
        <Pie data={JSON.parse(props.score)}/>
      </div>
    </div>
  );
}

const ProductSuggestions = (props) => {
  const { topDocuments } = props;

  if (Object.entries(topDocuments).length !== 0)
    return (
      <div>
        <Header as='h3' content='Suggestions' />
        <Grid container stackable>
          <CompareTwoDocuments topDocuments={topDocuments} />
        </Grid>
      </div>
    );
  else
    return "";
}

export default ProductSuggestions