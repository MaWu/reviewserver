import React from 'react'

class SearchBar extends React.Component {

  style = { width: "300px" }

  constructor(props) {
    super(props)
    
    this.initialState = {
      query: '',
    }
    this.state = this.initialState
  }
  
  handleChange = event => {
  const { name, value } = event.target
  
  this.setState({
    [name]: value,
  })
  }
  
  submitForm = () => {
    this.props.onSubmit(this.state)
    this.setState(this.initialState)
  }
  
  render() {
    const { query } = this.state;
    return (
      <form onSubmit={e => {e.preventDefault(); this.submitForm()}}>
        <label>Search</label>
        <input
          type="text"
          name="query"
          id="query"
          style={{ width: "500px", height: "30px" }}
          value={query}
          onChange={this.handleChange} />
        <input type="submit" value="Submit" onSubmit={this.submitForm}/>
      </form>
  );
  }
  
}


export default SearchBar;