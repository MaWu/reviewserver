import React, { Component } from 'react'
import { Grid, Segment, Header } from 'semantic-ui-react'

import Table from './Table'
import Form from './Form'
import SearchBar from './SearchBar'
import ProductSuggestions from './ProductSuggestions'
import SelectedItemsList from './SelectedItemsList'

class App extends Component {
        
	isLoggedIn = 'false';
	sessionID = "";
	
	state = { products: [], topDocuments: [] }

	selectProduct = index => {
		
		let copy = JSON.parse(JSON.stringify(this.state.products))
		if(copy[index].isSelected === "0")
			copy[index].isSelected = "1"
		else if(copy[index].isSelected === "1")
			copy[index].isSelected = "0"
		
		this.setState({
			products:copy
		})

		fetch('http://localhost:4000/productSelected', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				},
			body: JSON.stringify({
				sessionID: this.sessionID,
				product: copy[index],
				}),
			}).then();
		
	}

  /*
   * Login
   */

	loginSubmit = (props) => {
		const { username, password } = props;
		this.setState({
			username: username,
			password: password
		});
		
		fetch('http://localhost:4000/login', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				},
			body: JSON.stringify({
				user: username,
				password: password,
				}),
			}).then(response => response.json())
		.then(data => this.checkLogin(data));
	}
	
	checkLogin = (props) => {
		console.log(props.loginResult + " : " + props.sessionID);
		
		if(props.loginResult === 'true')
		{
			console.log('You are logged in now!')
			this.isLoggedIn = props.loginResult
			this.sessionID = props.sessionID
			this.getProducts()
		}
		if(props.loginResult === 'false')
		{
			console.log('Login failed!')
		}
	}

	/* 
	 * Seach
	 */

	searchSubmit = (props) => {
		const { query } = props;
		this.setState({
			query: query
		});

		fetch('http://localhost:4000/search', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
			},
			body: JSON.stringify({
				query: query,
			}),
		}).then(response => response.json())
			.then(data => this.searchResult(data));
	}

	searchResult = (props) => {
		console.log(props.querySuccessful + " : " + props.response);

		this.setState({
			topDocuments: []
		})

		if (props.querySuccessful === 'true') {
			console.log('Search successful!')
			this.setState({
				topDocuments: props.response
			})
			console.log('new state ' + this.state.topDocuments)
			this.forceUpdate()
		}
		if (props.querySuccessful === 'false') {
			console.log('Search failed!')
		}
	}

	getProducts = (props) => {
		fetch('http://localhost:4000/getProducts', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				},
			body: JSON.stringify({
				sessionID: this.sessionID
				}),
			}).then(response => response.json())
			.then(data => ( this.setProducts(data)    ) );
	}
	
	setProducts = (props) => {
		
		this.setState({
			products:props
		})
	}
	
	render() {
		
		if(this.isLoggedIn !== 'true')
			return (
				<div className="container">

					<div className="searchBar">
						<SearchBar onSubmit={this.searchSubmit} />
					</div>
					<div>
						<ProductSuggestions topDocuments={this.state.topDocuments} />
					</div>
				</div>
			)
		else
			return (
				<div id="parent">
					<Header as='h2' content='Login' />
					<div className="form">
						<Form onSubmit={this.loginSubmit} />
					</div>
				</div>
			)
		}
	}

export default App